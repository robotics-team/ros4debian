# ROS4Debian

ros4debian is a python3 program to convert deb packages of Ubuntu sources from the ROS project to their equivalent in debian but using the [upstream packages](http://wiki.ros.org/UpstreamPackages). The list of the packages and their status is [here](https://wiki.debian.org/DebianScience/Robotics/ROS/Packages).

## Installation

To use the program, a few steps must be done.

1. Add ROS sources.

`sudo sh -c 'echo "deb-src [signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros/ubuntu focal main" > /etc/apt/sources.list.d/ros-latest.list'`

2. Add the ROS key to obtain the sources.

`sudo apt install curl # if you haven't already installed curl`  
`curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | gpg --no-default-keyring --keyring ./ros-archive-keyring.gpg --import -`

3. Update the indexes.

`sudo apt update`

4. Clone this repo as usual.

## Using it

Once you have cloned the repo, just run:

`./ros4debian.py $myros-noetic-package`

where _$myros-noetic-package_ is the package do you want to build. The complete [list](https://salsa.debian.org/lepalom/ros4debian/-/blob/master/dataset/packages_osrf_rosworld.list) of packages is located in the dataset directory. 

When you run the program, it will download the sources from the ROS servers, rewrite the debian directory (control, changelog and rules) to use the Debian equivalent versions. After that, the user can run debuild to build the package. Pay attention that to build the package, you must have the build dependencies installed.

## How it works

The program first check if the requested package is in the list of possible candidates. If it is, then create a directory (if it doesn't exist) called _packages_  and a folder there with the Debian equivalent name (without noetic part). Download the sources and rewrite the debian directory.

If it is not there, then check is the package has been ported to Debian. If it is the case, then print which packages the user must install as an alternative. 

If it is not in any list, then shows that the package cannot be processed.

## Status

The program has processed 1522 packages without any error. It doesn't mean that that packages can be built without any problem. More work is needed, and an order list to build the archive must be created.

The debian directory is very rudimentary (as the original bloom original one) and some improvements can be done.


lpa 

