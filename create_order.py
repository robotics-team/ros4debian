#!/usr/bin/python3

import yaml
import debian.deb822
import re
import sys

# Function that from a string of packages
# returns a list of items of packages

def convert_to_package_list(data):
  data = data.split(",")
  new_data = []
  for item in data:
    if(item.startswith(' ')): # remove first if is ' ' 
      new_data.append(item[1:])
    else:
      new_data.append(item)
    #endif
  #endfor
  return new_data
#enddef

# function that return a list with name and version if exists
def split_pkg_version(package):
  ver = re.findall(r'\(.*?\)', package) # there's only one

  if len(ver)==0:
    pkg_version = [package,'']
  else:
    version = ver[0]
    name = package.replace(version, '')
    name = name.replace(' ','')
    pkg_version = [name,version]
  return pkg_version

def calculate_sum(row):
    ssum = 0;
    for j in range (1,dim):
        ssum = ssum + order_matrix[row][j]
    return ssum
#enddef

def calculate_dep(col):
    ssum = 0;
    for i in range (1,dim):
        ssum = ssum + order_matrix[i][col]
    return ssum
#enddef


list_package_to_work = sys.argv[1]

if not list_package_to_work:
    sys.exit("create order needs a list of packages")
else:
    stream = open(list_package_to_work, 'r')


pkg_list = stream.read().splitlines()
pkg_list.insert(0,'sum')

num_package = len(pkg_list)

print("We have",str(num_package - 1),"packages")

# initializing N
dim = num_package 

#dim = 10
# printing dimension
print("The dimension : " + str(dim))

order_matrix = [([0]*dim) for i in range(dim)]

# set 1 in the diagonal
for i in range(dim):
    order_matrix[i][i] = 1

  
#filling the matrix
for idx in range(1,dim):
  value = pkg_list[idx]
  controlf = "packages/"+value+"/"+value+"/debian/control"
  #controlf = "packages/ros-moveit/debian/control"
  
  for paragraph in debian.deb822.Deb822.iter_paragraphs(open(controlf)):
    for field, data in paragraph.items():
      if field == 'Build-Depends':
        ndata = convert_to_package_list(data)
        #print("Size of list:",len(ndata))        
        for b_dep in ndata:
          package = split_pkg_version(b_dep)
          package_name = str(package[0])
          package_version = str(package[1])
          #print(package_name)
          if package_name in pkg_list:
            pkg_num = pkg_list.index(package_name)
            #print(package_name)
            order_matrix[idx][pkg_num] = 1
          #endif
        #endfor
      #endif
    #endfor
  #endfor
#endfor
  

# some data to check

max_rdep = 0
pkg_rdep = 0
max2_rdep = 0
pkg2_rdep = 0

for j in range(1,dim):
  order_matrix[0][j] = calculate_dep(j)
  
  if order_matrix[0][j] > max_rdep:
      max2_rdep = max_rdep
      pkg2_rdep = pkg_rdep
      max_rdep = order_matrix[0][j]
      pkg_rdep = j
      #print("sum:",sum)
    #endif

print("The max r_dep is:", pkg_list[pkg_rdep],"with", max_rdep)
print("The_max2 r_dep is:", pkg_list[pkg2_rdep],"with", max2_rdep)
    
  
# iteration to print the order
level = 0
sum = 1 # first case
min = 200
drop = 1
while drop > 0:
  drop = 0
  print("########################################################")
  print("#PackageLevel:" + str(level))
  drop_list = []
  for i in range(1,dim):
    order_matrix[i][0] = calculate_sum(i)
    if order_matrix[i][0] > sum:
      sum = order_matrix[i][0]
      #print("sum:",sum)
    #endif
    if order_matrix[i][0] < min:
      min = order_matrix[i][0]
      #print("min:",min)
    #endif
    
    if order_matrix[i][0] == 1:
      drop = drop + 1
      drop_list.append(i)
      order_matrix[i][i] = 0
      #print(pkg_osrf_list[i])
      #for k in range(1,dim):
      #  order_matrix[k][i] = 0
      
    #endif
  #endfor
  # eliminate all the pkg with sum = 0
  # eliminate all the pkg in list
  for i in drop_list:
    print(pkg_list[i])
    for k in range(1,dim):
      order_matrix[k][i] = 0

  level = level + 1
  min = 200
#endwhile
